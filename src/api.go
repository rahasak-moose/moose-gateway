package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

func initApi() {
	// router
	r := mux.NewRouter()
	r.HandleFunc("/api/identities", apiIdentity).Methods("POST")
	r.HandleFunc("/api/slices", apiSlice).Methods("POST")

	// handlers
	origins := handlers.AllowedOrigins([]string{"*"})
	headers := handlers.AllowedHeaders([]string{"Authorization", "Bearer", "Content-Type"})
	methods := handlers.AllowedMethods([]string{"GET", "POST", "OPTIONS"})

	// start server with CORS
	err := http.ListenAndServe(":7654", handlers.CORS(origins, headers, methods)(r))
	if err != nil {
		log.Printf("ERROR: fail init http server, %s", err.Error)
		os.Exit(1)
	}
}

func apiIdentity(w http.ResponseWriter, r *http.Request) {
	// read body
	data, _ := ioutil.ReadAll(r.Body)
	defer r.Body.Close()

	log.Printf("INFO: request account contract %s", string(data))

	// unmarshal json
	jsonMap := make(map[string]interface{})
	err := json.Unmarshal(data, &jsonMap)
	if err != nil {
		log.Printf("ERROR: fail to unmarshla json, %s", err.Error())
		response(w, "Invalid request json", 400)
		return
	}

	msgTyp := fmt.Sprintf("%s", jsonMap["messageType"])
	if msgTyp == "create" || msgTyp == "register" || msgTyp == "connect" || msgTyp == "resetPassword" {
		// auth requests
		// call blockchian to handle account auth
		resp, status := post(data, apiConfig.identityApi)
		response(w, resp, status)
		return
	}

	if msgTyp == "addRole" || msgTyp == "activate" || msgTyp == "deactivate" || msgTyp == "search" || msgTyp == "get" ||
		msgTyp == "updateDevice" || msgTyp == "updateAccountNo" || msgTyp == "changePassword" || msgTyp == "resetPasswordConfirm" ||
		msgTyp == "updateAnswers" {
		// handle auth if enable
		if featureToggleConfig.enableVerifyToken == "yes" {
			execer := fmt.Sprintf("%s", jsonMap["execer"])
			err = verifyToken(execer, readToken(r))
			if err != nil {
				// unauthorized
				log.Printf("ERROR: fail to verify auth token, %s", err.Error())
				response(w, "Fail to verify Bearer", 401)
				return
			}
		}

		// call blockchian to handle account
		resp, status := post(data, apiConfig.identityApi)
		response(w, resp, status)
	}
}

func apiSlice(w http.ResponseWriter, r *http.Request) {
	// read body
	data, _ := ioutil.ReadAll(r.Body)
	defer r.Body.Close()

	log.Printf("INFO: request peer contract %s", string(data))

	// unmarshal json
	jsonMap := make(map[string]interface{})
	err := json.Unmarshal(data, &jsonMap)
	if err != nil {
		log.Printf("ERROR: fail to unmarshla json, %s", err.Error())
		response(w, "Invalid request json", 400)
		return
	}

	// handle auth if enable
	if featureToggleConfig.enableVerifyToken == "yes" {
		execer := fmt.Sprintf("%s", jsonMap["execer"])
		err := verifyToken(execer, readToken(r))
		if err != nil {
			// unauthorized
			log.Printf("ERROR: fail to verify auth token, %s", err.Error())
			response(w, "Fail to verify Bearer", 401)
			return
		}
	}

	// call blockchain to handle trace
	resp, status := post(data, apiConfig.qualificationApi)
	response(w, resp, status)
}

func response(w http.ResponseWriter, resp string, status int) {
	w.WriteHeader(status)
	w.Header().Set("Content-Type", "application/json")
	io.WriteString(w, string(resp))
}

func readToken(r *http.Request) string {
	// Get token from the Authorization header
	// format: Authorization: Bearer <token>
	tokens, ok := r.Header["Authorization"]
	if ok && len(tokens) >= 1 {
		token := tokens[0]
		token = strings.TrimPrefix(token, "Bearer ")
		return token
	}

	// format: Bearer: <token>
	tokens, ok = r.Header["Bearer"]
	if ok && len(tokens) >= 1 {
		token := tokens[0]
		return token
	}

	return ""
}
