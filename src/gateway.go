package main

func main() {
	// setup logging
	initLogger()

	// first init key pair
	initCrypto()

	// start http server
	initApi()
}
